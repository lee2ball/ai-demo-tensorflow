# Flask with tensorflow object detection demo /Flask和TensorFlow框架的物体检测demo

## 核心服务代码

Since flask is very simple and wroted by python, we build it with only a few lines of code.
flask是非常简洁易用的pyhton框架，我们只需要几行代码就可以搭建项目

This function receive base64 encoded image from front end page, converted it to PIL Image, then do the object detection step.
下面这个函数处理了来自前端页面的图像base64数据，把它转换成为了PIL图像，然后进行物体检测
```
@app.route('/api/', methods=["POST"])
def main_interface():
    response = request.get_json()
    data_str = response['image']
    point = data_str.find(',')
    base64_str = data_str[point:]  # remove unused part like this: "data:image/jpeg;base64,"

    image = base64.b64decode(base64_str)       
    img = Image.open(io.BytesIO(image))

    if(img.mode!='RGB'):
        img = img.convert("RGB")
    
    # convert to numpy array.
    img_arr = np.array(img)

    # do object detection in inference function.
    results = inference(sess, detection_graph, img_arr, conf_thresh=0.5)
    print(results)

    return jsonify(results)
```

## How to run.
 Open the server with 
```
python app.py

```
The server is setup on port 8001.


### Docker Build：

If you want to build your own docker image, you can modify the Dockerfile and execute the command:

`docker build -t flask-object-detection:1.0 .`

`docker run -p 8001:8001  -d flask-object-detection:1.0`

