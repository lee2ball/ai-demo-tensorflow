FROM python:3.7.17-bullseye

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple --no-cache-dir -r requirements.txt

COPY . .
CMD ["python", "./app.py"]
